$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // các combo Pizza
    const gCOMBO_SMALL = "Small";
    const gCOMBO_MEDIUM = "Medium";
    const gCOMBO_LARGE = "Large";
    // các loại pizza
    const gPIZZA_OCEAN_MANIA = "Seafood";
    const gPIZZA_HAWAIIAN = "Hawaii";
    const gPIZZA_BACON = "Bacon";
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365";
    var gDataOrder;
    var gOrderedObjRequest;
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //gán hàm xử lý sự kiện cho nút chọn kích cỡ small
    $("#btn-small").on("click", onBtnSmallSizeClick);
    //gán hàm xử lý sự kiện cho nút chọn kích cỡ medium
    $("#btn-medium").on("click", onBtnMediumSizeClick);
    //gán hàm xử lý sự kiện cho nút chọn kích cỡ large
    $("#btn-large").on("click", onBtnLargeSizeClick);
    //gán hàm xử lý sự kiện cho nút chọn loại pizza ocean mania
    $("#btn-ocean-mania").on("click", onBtnOceanManiaClick);
    //gán hàm xử lý sự kiện cho nút chọn loại pizza hawaii
    $("#btn-hawaii").on("click", onBtnHawaiiClick);
    //gán hàm xử lý sự kiện cho nút chọn loại pizza bacon
    $("#btn-bacon").on("click", onBtnBaConClick);
    //gán sự kiện cho nút Gửi
    $("#btn-send-order").on("click", onBtnSendClick);
    //gán sự kiện cho nút Tạo đơn(trên modal)
    $("#btn-create-order-modal").on("click", onBtnCreateOrderClick);
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm load trang dc gọi khi f5
    function onPageLoading() {
        console.log("%c Load trang khi f5...", "color:red;");
        callAPIAndLoadDrinkSelect();
    }
    // hàm được gọi khi bấm nút chọn kích cỡ S
    function onBtnSmallSizeClick() {
        "use strict";
        // gọi hàm đổi màu nút
        // chỉnh combo, combo được chọn là Small, đổi nút, và đánh dấu
        changeComboButtonColor(gCOMBO_SMALL);
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }
    // hàm được gọi khi bấm nút chọn kích cỡ M
    function onBtnMediumSizeClick() {
        "use strict";
        // gọi hàm đổi màu nút
        // chỉnh combo, combo được chọn là Medium, đổi nút, và đánh dấu
        changeComboButtonColor(gCOMBO_MEDIUM);
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }
    // hàm được gọi khi bấm nút chọn kích cỡ L
    function onBtnLargeSizeClick() {
        "use strict";
        // gọi hàm đổi màu nút
        // chỉnh combo, combo được chọn là Large, đổi nút, và đánh dấu
        changeComboButtonColor(gCOMBO_LARGE);
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }
    // hàm được gọi khi bấm nút chọn loại pizza Ocean Mania
    function onBtnOceanManiaClick() {
        "use strict";
        // gọi hàm đổi màu nút
        changeTypeButtonColor(gPIZZA_OCEAN_MANIA);
        // ghi loại pizza đã chọn ra console
        console.log("Loại pizza đã chọn: " + gPIZZA_OCEAN_MANIA);
    }
    // hàm được gọi khi bấm nút chọn loại pizza Hawai
    function onBtnHawaiiClick() {
        "use strict";
        // gọi hàm đổi màu nút
        changeTypeButtonColor(gPIZZA_HAWAIIAN);
        // ghi loại pizza đã chọn ra console
        console.log("Loại pizza đã chọn: " + gPIZZA_HAWAIIAN);
    }
    // hàm được gọi khi bấm nút chọn loại pizza BaCon
    function onBtnBaConClick() {
        "use strict";
        // gọi hàm đổi màu nút
        changeTypeButtonColor(gPIZZA_BACON);
        // ghi loại pizza đã chọn ra console
        console.log("Loại pizza đã chọn: " + gPIZZA_BACON);
    }
    function onBtnSendClick() {
        //tạo obj chứa data order
        var vOrderObj = {
            kichCoCombo: null,
            loaiPizza: null,
            fullName: "",
            email: "",
            phone: "",
            address: "",
            message: "",
            maGiamGia: "",
            phanTramGiamGia: "",
            drink: "",
            priceAnnualVND: function () {
                var vTotal = this.kichCoCombo.priceVND * (1 - this.phanTramGiamGia / 100);
                return vTotal;
            }
        };
        // Bước 1: Đọc
        getOrder(vOrderObj);
        //Bước 2: kiểm tra
        var vKetQuaKiemTra = checkValidate(vOrderObj);
        if (vKetQuaKiemTra) {
            //Bước 3: Hiển thị
            showOrderInfoToModal(vOrderObj);
            $("#order-modal").modal("show");
        }
        gDataOrder = vOrderObj;
    }

    //hàm xử lý sự kiện nút Tạo đơn(trên modal)
    function onBtnCreateOrderClick() {
        // khai báo object order để chứa thông tin đặt hàng
        var vObjectRequest = {
            kichCo: gDataOrder.kichCoCombo.menuName,
            duongKinh: gDataOrder.kichCoCombo.duongKinhCM,
            suon: gDataOrder.kichCoCombo.suonNuong,
            salad: gDataOrder.kichCoCombo.saladGr,
            loaiPizza: gDataOrder.loaiPizza,
            idVourcher: gDataOrder.maGiamGia,
            giamGia: gDataOrder.phanTramGiamGia,
            idLoaiNuocUong: gDataOrder.drink,
            soLuongNuoc: gDataOrder.kichCoCombo.drinkAmount,
            hoTen: gDataOrder.fullName,
            thanhTien: gDataOrder.priceAnnualVND(),
            email: gDataOrder.email,
            soDienThoai: gDataOrder.phone,
            diaChi: gDataOrder.address,
            loiNhan: gDataOrder.message
        }
        callApiToPostOrderedDataToServer(vObjectRequest);
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //function get combo (lấy menu được chọn)
    //function trả lại một đối tượng combo (menu được chọn) được tham số hóa
    function getComboSelected(
        paramMenuName,
        paramDuongKinhCM,
        paramSuonNuong,
        paramSaladGr,
        paramDrinkAmount,
        paramPriceVND
    ) {
        var vSelectedMenu = {
            menuName: paramMenuName, // S, M, L
            duongKinhCM: paramDuongKinhCM,
            suonNuong: paramSuonNuong,
            saladGr: paramSaladGr,
            drinkAmount: paramDrinkAmount,
            priceVND: paramPriceVND,
            displayInConsoleLog() {
                console.log("%cMENU COMBO - .........", "color:blue");
                console.log(this.menuName); //this = "đối tượng này"
                console.log("duongKinhCM: " + this.duongKinhCM);
                console.log("suongNuong: " + this.suonNuong);
                console.log("saladGr: " + this.saladGr);
                console.log("drink:" + this.drinkAmount);
                console.log("priceVND: " + this.priceVND);
            },
        };
        return vSelectedMenu;
    }
    // hàm đổi mầu nút khi chọn combo
    function changeComboButtonColor(paramCombo) {
        var vBtnSmall = $("#btn-small"); // truy vấn nút chọn kích cỡ small
        var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
        var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large

        if (paramCombo === gCOMBO_SMALL) {
            //nếu chọn menu Small thì thay màu nút chọn kích cỡ Small bằng màu xanh, hai nút kia cam
            // đổi giá trị data-is-selected-menu
            vBtnSmall.css("background-color", "greenyellow");
            vBtnSmall.attr("data-is-selected-menu", "Y");
            vBtnMedium.css("background-color", "orange");
            vBtnMedium.attr("data-is-selected-menu", "N");
            vBtnLarge.css("background-color", "orange");
            vBtnLarge.attr("data-is-selected-menu", "N");

        } else if (paramCombo === gCOMBO_MEDIUM) {
            //nếu chọn menu Medium thì thay màu nút chọn kích cỡ Medium bằng màu xanh, hai nút kia cam
            // đổi giá trị data-is-selected-menu
            vBtnSmall.css("background-color", "orange");
            vBtnSmall.attr("data-is-selected-menu", "N");
            vBtnMedium.css("background-color", "greenyellow");
            vBtnMedium.attr("data-is-selected-menu", "Y");
            vBtnLarge.css("background-color", "orange");
            vBtnLarge.attr("data-is-selected-menu", "N");
        } else if (paramCombo === gCOMBO_LARGE) {
            //nếu chọn menu Large thì thay màu nút chọn kích cỡ Large bằng màu xanh, hai nút kia cam
            // đổi giá trị data-is-selected-menu
            vBtnSmall.css("background-color", "orange");
            vBtnSmall.attr("data-is-selected-menu", "N");
            vBtnMedium.css("background-color", "orange");
            vBtnMedium.attr("data-is-selected-menu", "N");
            vBtnLarge.css("background-color", "greenyellow");
            vBtnLarge.attr("data-is-selected-menu", "Y");
        }
    }

    // hàm đổi mầu nút khi chọn loại pizza
    function changeTypeButtonColor(paramType) {
        var vBtnOceanMania = $("#btn-ocean-mania");
        var vBtnHawaii = $("#btn-hawaii");
        var vBtnBaCon = $("#btn-bacon");

        if (paramType === gPIZZA_OCEAN_MANIA) {
            vBtnOceanMania.css("background-color", "greenyellow");
            vBtnOceanMania.attr("data-is-selected-pizza", "Y");
            vBtnHawaii.css("background-color", "orange");
            vBtnHawaii.attr("data-is-selected-pizza", "N");
            vBtnBaCon.css("background-color", "orange");
            vBtnBaCon.attr("data-is-selected-pizza", "N");
        } else if (paramType === gPIZZA_HAWAIIAN) {
            vBtnOceanMania.css("background-color", "orange");
            vBtnOceanMania.attr("data-is-selected-pizza", "N");
            vBtnHawaii.css("background-color", "greenyellow");
            vBtnHawaii.attr("data-is-selected-pizza", "Y");
            vBtnBaCon.css("background-color", "orange");
            vBtnBaCon.attr("data-is-selected-pizza", "N");
        } else if (paramType === gPIZZA_BACON) {
            vBtnOceanMania.css("background-color", "orange");
            vBtnOceanMania.attr("data-is-selected-pizza", "N");
            vBtnHawaii.css("background-color", "orange");
            vBtnHawaii.attr("data-is-selected-pizza", "N");
            vBtnBaCon.css("background-color", "greenyellow");
            vBtnBaCon.attr("data-is-selected-pizza", "Y");
        }
    }

    // API lấy danh sách đồ uống, để load vào Select đồ uống
    function callAPIAndLoadDrinkSelect() {
        //gọi API để lấy data đồ uống
        $.ajax({
            url: gBASE_URL + "/drinks",
            type: "GET",
            success: function (paramDrink) {
                console.log("Json Obj drinks: ", paramDrink);
                handleDrinkList(paramDrink); //hiển thị data vào drink select
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //hàm tạo option cho select drink
    function handleDrinkList(paramDrink) {
        $.each(paramDrink, function (i, item) {
            $("#select-drink").append($("<option>", {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }));
        });
    }

    // hàm thu thập(đọc) thông tin order
    function getOrder(paramOrder) {
        var vBtnSmall = $("#btn-small"); // truy vấn nút chọn kích cỡ small
        var vComboSmallDaChon = vBtnSmall.attr("data-is-selected-menu");
        var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
        var vComboMediumDaChon = vBtnMedium.attr("data-is-selected-menu");
        var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large
        var vComboLargeDaChon = vBtnLarge.attr("data-is-selected-menu");
        var vSelectedMenuStructure = getComboSelected("", 0, 0, 0, 0, 0);
        if (vComboSmallDaChon === "Y") {
            vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
        }
        else if (vComboMediumDaChon === "Y") {
            vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
        }
        else if (vComboLargeDaChon === "Y") {
            vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
        }
        paramOrder.kichCoCombo = vSelectedMenuStructure;

        var vBtnOceanMania = $("#btn-ocean-mania");
        var vPizzaOceanManiaDaChon = vBtnOceanMania.attr("data-is-selected-pizza");
        var vBtnHawaii = $("#btn-hawaii");
        var vPizzaHawaiiDaChon = vBtnHawaii.attr("data-is-selected-pizza");
        var vBtnBacon = $("#btn-bacon");
        var vPizzaBaconDaChon = vBtnBacon.attr("data-is-selected-pizza");
        var vLoaiPizzaSelected = "";
        if (vPizzaOceanManiaDaChon === "Y") {
            vLoaiPizzaSelected = gPIZZA_OCEAN_MANIA;
        }
        else if (vPizzaHawaiiDaChon === "Y") {
            vLoaiPizzaSelected = gPIZZA_HAWAIIAN;
        }
        else if (vPizzaBaconDaChon === "Y") {
            vLoaiPizzaSelected = gPIZZA_BACON;
        }
        paramOrder.loaiPizza = vLoaiPizzaSelected;

        var vElementInputFullName = $("#inp-fullname");
        paramOrder.fullName = vElementInputFullName.val().trim();

        var vElementInputEmail = $("#inp-email");
        paramOrder.email = vElementInputEmail.val().trim();

        var vElementInputPhone = $("#inp-phone");
        paramOrder.phone = vElementInputPhone.val().trim();

        var vElementInputAddress = $("#inp-address");
        paramOrder.address = vElementInputAddress.val().trim();

        var vElementInputMessage = $("#inp-message");
        paramOrder.message = vElementInputMessage.val().trim();

        var vElementSelectDrink = $("#select-drink");
        paramOrder.drink = vElementSelectDrink.val();

        var vElementInputVoucherID = $("#inp-voucher-id");
        paramOrder.maGiamGia = vElementInputVoucherID.val().trim();
    }

    // ham kiem tra thong tin dat hang
    function checkValidate(paramOrder) {
        if (paramOrder.kichCoCombo.menuName === "") {
            alert("Hãy chọn combo pizza...");
            return false;
        }
        if (paramOrder.loaiPizza === "") {
            alert("Hãy chọn loại pizza...");
            return false;
        }
        if (paramOrder.drink === "all drink") {
            alert("Hãy chọn đồ uống...");
            return false;
        }
        if (paramOrder.fullName === "") {
            alert("Hãy nhập họ và tên!");
            return false;
        }
        if (isEmail(paramOrder.email) == false) {
            return false;
        }
        if (paramOrder.phone === "") {
            alert("Hãy nhập số điện thoại!");
            return false;
        }
        if (isNaN(paramOrder.phone)) {
            alert("Số điện thoại phải là dãy số!");
            return false;
        }
        if (paramOrder.address === "") {
            alert("Hãy nhập địa chỉ!");
            return false;
        }
        return true;
    }

    //hàm kiểm tra email
    function isEmail(paramEmail) {
        if (paramEmail < 3) {
            alert("Nhập email...");
            return false;
        }
        if (paramEmail.indexOf("@") === -1) {
            alert("Email phải có ký tự @");
            return false;
        }
        if (paramEmail.startsWith("@") === true) {
            alert("Email không bắt đầu bằng @");
            return false;
        }
        if (paramEmail.endsWith("@") === true) {
            alert("Email không kết thúc bằng @");
            return false;
        }
        return true;
    }

    //hàm gọi api để check mã giảm giá
    function callApiToGetVoucherObj(paramVoucherId) {
        "use strict";
        var vVoucherObj = null;
        var vVoucherId = $("#inp-voucher-id");
        paramVoucherId = vVoucherId.val().trim();
        // nếu mã giảm giấ đã nhập, tạo xmlHttp  request và gửi về server
        if (paramVoucherId !== "") {
            $.ajax({
                url: gBASE_URL + "/voucher_detail/" + paramVoucherId,
                type: "GET",
                async: false,
                success: function (paramRes) {
                    vVoucherObj = paramRes;
                    console.log(vVoucherObj);
                },
                error: function () {
                    // không nhận lại được data do vấn đề gì đó: khả năng mã voucher không dúng
                    console.log("Không tìm thấy voucher " + paramVoucherId);
                    alert("Mã giảm giá không tồn tại!");
                    return null;
                }
            });
            return vVoucherObj;
        }
    }
    // hàm show thông tin đặt hàng vào modal
    function showOrderInfoToModal(paramOrderObj) {
        var vVoucherObj = callApiToGetVoucherObj(paramOrderObj.maGiamGia);
        if (vVoucherObj == null) {
            paramOrderObj.phanTramGiamGia = 0;
        } else {
            paramOrderObj.phanTramGiamGia = vVoucherObj.phanTramGiamGia;
        }
        $("#modal-inp-fullname").val(paramOrderObj.fullName);
        $("#modal-inp-phone").val(paramOrderObj.phone);
        $("#modal-inp-address").val(paramOrderObj.address);
        $("#modal-inp-message").val(paramOrderObj.message);
        $("#modal-inp-voucherid").val(paramOrderObj.maGiamGia);
        $("#div-info-detail").html(
            "Xác nhận: " + paramOrderObj.fullName + ", " + paramOrderObj.phone + ", " + paramOrderObj.address + "<br>"
            + "Menu " + paramOrderObj.kichCoCombo.menuName + ", sườn nướng " + paramOrderObj.kichCoCombo.suonNuong + ", nước " + paramOrderObj.kichCoCombo.drinkAmount + " ..." + "<br>"
            + "Loại pizza: " + paramOrderObj.loaiPizza + ", Giá: " + paramOrderObj.kichCoCombo.priceVND + " vnd" + ", Mã giảm giá: " + paramOrderObj.maGiamGia + "<br>"
            + "Phải thanh toán: " + paramOrderObj.priceAnnualVND() + " vnd (" + "giảm giá " + paramOrderObj.phanTramGiamGia + "%)"
        );
    }
    //hàm call api to post ordered data to server
    function callApiToPostOrderedDataToServer(paramOrderedObj) {
        $.ajax({
            url: gBASE_URL + "/orders",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderedObj),
            success: function (paramRes) {
                console.log(paramRes);
                gOrderedObjRequest = paramRes;
                handleCreateOrderSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
    //hàm xử lý front-end khi tạo order thành công
    function handleCreateOrderSuccess() {
        $("#inp-modal-ordercode").val(gOrderedObjRequest.orderCode);
        $("#order-modal").modal("hide");
        $("#result-order-modal").modal("show");
        resetSendOrderForm();
    }
    //hàm xóa trắng form send order
    function resetSendOrderForm() {
        $("#inp-fullname").val("");
        $("#inp-email").val("");
        $("#inp-phone").val("");
        $("#inp-address").val("");
        $("#inp-voucher-id").val("");
        $("#inp-message").val("");
    }
});